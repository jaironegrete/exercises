package dragonCurveAlgorithm;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(1);
		int cont = 1;
		while(cont != 12) {
			if(cont == 1) {
				System.out.println(numbers.get(0));
			}else {
				boolean print1 = true;
				int contInteger = 1;
				List<Integer> tempNumbers = new ArrayList<>();
				for (Integer integer : numbers) {
					tempNumbers.add(print1 ? 1 : 0);
					System.out.print(print1 ? 1 : 0);
					print1 = !print1;
					tempNumbers.add(integer);
					System.out.print(integer);
					if(numbers.size() == contInteger) {
						tempNumbers.add(print1 ? 1 : 0);
						System.out.print(print1 ? 1 : 0);
						print1 = !print1;
						numbers = tempNumbers;
						tempNumbers = new ArrayList<>();
						System.out.println("|");
						break;
					}else {
						contInteger++;
					}
				}
			}
			cont++;
		}
	}
}
